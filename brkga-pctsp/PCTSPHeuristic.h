#ifndef PCTSPHEURISTIC_H
#define PCTSPHEURISTIC_H

#include <vector>
#include <cstdlib>
#include <time.h>
#include <algorithm>
#include "PCTSPInstance.h"

class PCTSPHeuristic
{
    public:
        PCTSPHeuristic();
        virtual ~PCTSPHeuristic();

        std::vector< unsigned > seqAdd(const PCTSPInstance& instance);
        std::vector< unsigned > seqDrop(const PCTSPInstance& instance, std::vector< unsigned > route);

    private:

};

#endif // PCTSPHEURISTIC_H
