/*
 * 	PTSPDecoder.cpp
 *
 *  Based on TSPDecoder.cpp from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#include "PCTSPDecoder.h"

PCTSPDecoder::PCTSPDecoder(const PCTSPInstance& _instance) : instance(_instance) {
}

PCTSPDecoder::~PCTSPDecoder() {
}

double PCTSPDecoder::decode(const std::vector< double >& chromosome) const {
	// 1) Solve the problem (i.e., create a tour out of this chromosome):
	// Avoids race conditions by making sure we have a single TSPSolver for each thread calling
	// ::decode (as long as TSPSolver does not make use of 'static' or other gimmicks):
	PCTSPSolver solver(instance, chromosome);

	// 2) Extract the fitness (tour cost):
	const unsigned fitness = solver.getTourCost();

	// 3) Return:
	return double(fitness);
}
