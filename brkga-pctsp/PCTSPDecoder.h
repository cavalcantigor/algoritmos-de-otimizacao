/*
 * 	PCTSPDecoder.h
 *
 *  Based on TSPDecoder.h from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#ifndef PCTSPDecoder_H
#define PCTSPDecoder_H

#include "PCTSPSolver.h"
#include "PCTSPInstance.h"

class PCTSPDecoder {
public:
	PCTSPDecoder(const PCTSPInstance& instance);
	virtual ~PCTSPDecoder();

	// Decodes a chromosome into a solution to the TSP:
	double decode(const std::vector< double >& chromosome) const;

private:
	const PCTSPInstance& instance;
};

#endif
