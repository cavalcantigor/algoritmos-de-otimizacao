/*
 * 	PCTSPInstance.cpp
 *
 *  Based on TSPInstance.cpp from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#include "PCTSPInstance.h"

PCTSPInstance::PCTSPInstance(const std::string& instanceFile) throw(PCTSPInstance::Error) : prizes(0),
	penalties(0), costs(0), nNodes(0), prize_max(0), prize_min(0){
	std::ifstream fin(instanceFile.c_str());
	if(!fin) { throw Error("PCTSPInstance: Cannot open input file."); }

	std::string line;
	std::string token;

	try {
		// reading white lines and then prizes
		std::getline(fin, line);
		std::getline(fin, line);
		std::getline(fin, line);
		std::getline(fin, line);
		readPrizes(line);

		// reading white lines and then penalties
		std::getline(fin, line);
		std::getline(fin, line);
		std::getline(fin, line);
		readPenalties(line);

		// define nNodes - it could be prizes length or penalties length
		nNodes = (unsigned)prizes.size();

		// reading white lines and then costs matrix
		std::getline(fin, line);
		std::getline(fin, line);
		int i = 0;
		while(!fin.eof()) {
			std::getline(fin, line);
			readCosts(line, i);
			i++;
		}

		// Calculating min prize
        prize_min = (prize_max * 0.75);
	}
	catch(const Error& error) { throw error; }
}

PCTSPInstance::~PCTSPInstance() { }

unsigned PCTSPInstance::getNumNodes() const { return nNodes; }
unsigned PCTSPInstance::getPrizeMax() const { return prize_max; }
unsigned PCTSPInstance::getPrizeMin() const { return prize_min; }
const std::vector< int > PCTSPInstance::getPrizes() const { return prizes; }
const std::vector< int > PCTSPInstance::getPenalties() const { return penalties; }
const std::vector< std::vector< int > > PCTSPInstance::getCosts() const { return costs; }

void PCTSPInstance::readPrizes(const std::string& line) throw(PCTSPInstance::Error) {
	std::istringstream sin(line);

	int prize;
	prize_max = 0;
	while(sin >> prize){
		prizes.push_back(prize);
		prize_max += prize;
	}
}

void PCTSPInstance::readPenalties(const std::string& line) throw(PCTSPInstance::Error) {
	std::istringstream sin(line);

	int penalti;
	while(sin >> penalti){
		penalties.push_back(penalti);
	}
}

void PCTSPInstance::readCosts(const std::string& line, const int i) throw(PCTSPInstance::Error) {
	std::istringstream sin(line);

	// initializing inner vector
	costs.push_back(std::vector< int >(nNodes));

	int j = 0, cost;
	while(sin >> cost){
		costs[i][j] = cost;
		j++;
	}
}

