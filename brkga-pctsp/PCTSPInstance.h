/*
 * 	PCTSPInstance.h
 *
 *  Based on TSPInstance.h from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#ifndef PCTSPInstance_H
#define PCTSPInstance_H

#include <cmath>
#include <string>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <stdexcept>

class PCTSPInstance {
public:
	typedef std::runtime_error Error;

	PCTSPInstance(const std::string& instanceFile) throw(Error);
	virtual ~PCTSPInstance();

	// Getters:
	unsigned getNumNodes() const;
    unsigned getPrizeMax() const;
    unsigned getPrizeMin() const;
	const std::vector< int > getPrizes() const;
	const std::vector< int > getPenalties() const;
	const std::vector< std::vector< int > > getCosts() const;


private:
	std::vector< int > prizes;
	std::vector< int > penalties;
	std::vector< std::vector< int > > costs;

	unsigned nNodes;
	unsigned prize_max;
	unsigned prize_min;

	void readPrizes(const std::string& line) throw (Error);
	void readPenalties(const std::string& line) throw (Error);
	void readCosts(const std::string& line, const int i) throw(Error);
};

#endif
