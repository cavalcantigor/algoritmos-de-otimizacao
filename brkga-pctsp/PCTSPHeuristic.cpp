#include "PCTSPHeuristic.h"

PCTSPHeuristic::PCTSPHeuristic()
{
    //ctor
}

PCTSPHeuristic::~PCTSPHeuristic()
{
    //dtor
}

std::vector< unsigned > PCTSPHeuristic::seqAdd(const PCTSPInstance& instance){
	bool keep_going = true;

	std::vector< unsigned > route;
	std::vector< unsigned > unroute;

	unsigned node;
	unsigned prize_collected = 0;

	srand(time(NULL));

	route.push_back(0);
	for(unsigned int i = 1; i < instance.getNumNodes(); i++){
		unroute.push_back(i);
	}

	unsigned int beta_nodes = (instance.getNumNodes() * 0.3) - 1;
	unsigned int j = 0;
	for(unsigned int i = 1; i < beta_nodes; i++){
		 j = rand() % unroute.size();
		 route.push_back(unroute[j]);
		 unroute.erase(unroute.begin() + j);
	}

	while(keep_going){
		unsigned int position_j = 0, position_k = 0;
		double eco_best = 99999.9;

		if(!unroute.empty()){
			for(unsigned int k = 0; k < unroute.size(); k++){
				unsigned int route_size = route.size();

				for(unsigned int i = 0; i < route_size; i++){
					unsigned int j = 0;

					if(i == (route_size - 1)){
						j = 0;
					}else{
						j = i + 1;
					}

					int economy = 0;
					double eco_prize = 0.0;

					economy = instance.getCosts()[route[i]][unroute[k]]
							+ instance.getCosts()[unroute[k]][route[j]]
							- instance.getCosts()[route[i]][route[j]]
							- instance.getPrizes()[unroute[k]];

					eco_prize = economy;

					if(eco_prize < eco_best){
						// save positions
						position_j = j;
						position_k = k;
						// save node inserted
						node = unroute[k];
						// save best economy
						eco_best = eco_prize;
					}
				}
			}
			if(eco_best < 0 /*|| prize_collected < instance.getPrizeMin()*/){

				prize_collected += instance.getPrizes()[route[position_j]];

				route.insert(route.begin() + position_j, node);
				unroute.erase(unroute.begin() + position_k);
			}else{
				keep_going = false;
			}
		}else{
			keep_going = false;
		}
	}

	return route;
}

std::vector< unsigned > PCTSPHeuristic::seqDrop(const PCTSPInstance& instance, std::vector< unsigned > route){
    bool keep_going = true;

	std::vector< unsigned > unroute;

	unsigned prize_collected = 0;

	for(unsigned int i = 0; i < route.size(); i++){
        prize_collected += instance.getPrizes()[route[i]];
		if(std::find(route.begin(), route.end(), i) == route.end()){
			unroute.push_back(i);
		}
	}

	while(keep_going){
		unsigned int position_k = 0;
		double eco_best = 0;

		if(!route.empty()){
			for(unsigned int k = 0; k < route.size(); k++){

				unsigned int i, j;

                if(k == 0){
                    i = route.size() - 1;
                }else{
                    i = k - 1;
                }

                if(k == (route.size() - 1)){
                    j = 0;
                }else{
                    j = k + 1;
                }

                int economy = 0;
                double eco_prize = 0.0;

                economy = instance.getCosts()[route[i]][unroute[j]]
                        + instance.getPrizes()[route[k]]
                        - instance.getCosts()[route[i]][route[k]]
                        - instance.getCosts()[route[k]][route[j]];

                eco_prize = economy;

                if(eco_prize < eco_best){
                    // save positions
                    position_k = k;
                    // save best economy
                    eco_best = eco_prize;
                }

			}
			if(eco_best < 0 && ((prize_collected - instance.getPrizes()[route[position_k]]) >= instance.getPrizeMin())){

				prize_collected -= instance.getPrizes()[route[position_k]];

				unroute.push_back(route[position_k]);
				route.erase(route.begin() + position_k);
			}else{
				keep_going = false;
			}
		}else{
			keep_going = false;
		}
	}

	return route;
}
