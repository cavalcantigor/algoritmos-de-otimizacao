/*
 * 	PCTSPSolver.H
 *
 *  Based on TSPSolver.h from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#ifndef PCTSPSolver_H
#define PCTSPSolver_H

#include <list>
#include <limits>
#include <vector>
#include <algorithm>
#include <iostream>
#include "PCTSPInstance.h"

class PCTSPSolver {
public:
	// The constructor 'solves' the problem in O(n log n) by transforming the chromosome into
	// a tour (by getting a permutation out of the chromosome):
	PCTSPSolver();
	PCTSPSolver(const PCTSPInstance& instance, const std::vector< double >& chromosome);
	virtual ~PCTSPSolver();

	unsigned getTourCost() const;		// Returns the tour cost
	std::list< unsigned > getTour() const;	// Returns the tour (first node not copied in the end)

	unsigned int getCostFromTour(const PCTSPInstance& instance, std::vector< unsigned > tour);

private:
	typedef std::pair< double, unsigned > ValueKeyPair;

	unsigned cost;
	unsigned prize_collected;

	static const unsigned PENALTY_PRIZE_MIN = 999999;

	double beta;

	std::vector< ValueKeyPair > route;
	std::vector< ValueKeyPair > unroute;

	void initializeNormalDecoder(const PCTSPInstance& instance, const std::vector< double >& chromosome);
};

#endif
