/*
 * 	PCTSPSolver.cpp
 *
 *  Based on TSPSolver.cpp from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 */

#include "PCTSPSolver.h"

PCTSPSolver::PCTSPSolver() : cost(0), prize_collected(0), beta(0), route(0), unroute(0){

}

PCTSPSolver::PCTSPSolver(const PCTSPInstance& instance, const std::vector< double >& chromosome) :
		cost(0), prize_collected(0), beta(0.7), route(instance.getNumNodes()), unroute(0){
	// Assumes that instance.getNumNodes() == chromosome.size() and unroute a beta size of this
    initializeNormalDecoder(instance, chromosome);
}

void PCTSPSolver::initializeNormalDecoder(const PCTSPInstance& instance, const std::vector< double >& chromosome){

	// 1) Obtain a permutation out of the chromosome -- this will be the route:
	// prize max is also calculated
	for(unsigned i = 0; i < chromosome.size(); ++i) {
		route[i] = ValueKeyPair(chromosome[i], i);
	}

	// Here we sort 'rank', which will produce a permutation of [n] stored in ValueKeyPair::second:
	std::sort(route.begin(), route.end());

	// 4) Compute the cost of the route given by the permutation:
	prize_collected = instance.getPrizes()[route[0].second];
	for(unsigned i = 1; i < route.size(); ++i) {
		// Compute distance(i-1, i) in the permutation:
		const unsigned& source = route[i-1].second;
		const unsigned& destination = route[i].second;

		if(prize_collected >= instance.getPrizeMin()){
            // inserting to unroute
            ValueKeyPair node = ValueKeyPair(0.0f, 0);
            node.first = route[i].first;
            node.second = route[i].second;

            unroute.push_back(node);
            route[i].second = 999999;
		}else{
            cost += instance.getCosts()[source][destination];
            prize_collected += instance.getPrizes()[destination];
		}
	}

	// removing from route nodes that are in unroute
	while(true){
		const unsigned& count = route.size();
		if(route[count - 1].second == 999999){
			route.erase(route.end());
		}else{
			break;
		}
	}

	// Close the route:
	const unsigned& last = route.back().second;
	const unsigned& first = route.front().second;
	cost += instance.getCosts()[last][first];

	// 5) Compute the penalties given by unroute nodes:
	for(unsigned i = 0; i < unroute.size(); ++i) {
		cost += instance.getPenalties()[unroute[i].second];
	}

	// 6) Add penalty if prize min is not collected
	if(prize_collected < instance.getPrizeMin()){
		cost += PENALTY_PRIZE_MIN;
	}
}

PCTSPSolver::~PCTSPSolver() {
}

unsigned PCTSPSolver::getTourCost() const {
	return cost;
}

std::list< unsigned > PCTSPSolver::getTour() const {
	std::list< unsigned > tourSequence;

	for(unsigned i = 0; i < route.size(); ++i) { tourSequence.push_back(route[i].second); }

	return tourSequence;
}

unsigned int PCTSPSolver::getCostFromTour(const PCTSPInstance& instance, std::vector< unsigned > tour){

	unsigned int tour_cost = 0;
	unsigned int tour_prize_collected = instance.getPrizes()[tour[0]];

	for(unsigned i = 1; i < tour.size(); ++i) {
		const unsigned& source = tour[i-1];
		const unsigned& destination = tour[i];

		tour_cost += instance.getCosts()[source][destination];
		tour_prize_collected += instance.getPrizes()[destination];
	}


	const unsigned& last = tour.back();
	const unsigned& first = tour.front();

	tour_cost += instance.getCosts()[last][first];

	for(unsigned i = 0; i < instance.getNumNodes(); ++i) {
		if(std::find(tour.begin(), tour.end(), i) == tour.end()){
			tour_cost += instance.getPenalties()[i];
		}
	}

	if(tour_prize_collected < instance.getPrizeMin()){
		tour_cost += PENALTY_PRIZE_MIN;
	}

	return tour_cost;
}

