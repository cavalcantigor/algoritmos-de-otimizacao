/**
 * brkga-pctsp.cpp
 *  Based on brkga-tsp.cpp from brkgaApi examples.
 *  Created on: Nov 11, 2018
 *      Author: Igor Cavalcanti
 *
 */

//#include <iostream>
#include <algorithm>
#include "brkgaAPI/BRKGA.h"
#include "brkgaAPI/MTRand.h"

#include "PCTSPSolver.h"
#include "PCTSPDecoder.h"
#include "PCTSPInstance.h"
#include "PCTSPHeuristic.h"

int main(int argc, char* argv[]) {
	if(argc < 2) { std::cerr << "usage: <PCTSP - instance file>" << std::endl; return -1; }

	std::cout << "Welcome to the BRKGA-PCTSP.\nAn implementation of BRKGA to "
			<< "PCTSP problem."<< std::endl;

	const clock_t begin = clock();

	const std::string instanceFile = std::string(argv[1]);
	std::cout << "Instance file: " << instanceFile << std::endl;

	// Read the instance:
	PCTSPInstance instance(instanceFile); 	// initialize the instance
	std::cout << "Instance read; here's the info:"
			<< "\n\tDimension: " << instance.getNumNodes()
			<< std::endl;

	PCTSPHeuristic heuristics;
	PCTSPSolver solver;
	PCTSPDecoder decoder(instance);		// initialize the decoder

	const long unsigned rngSeed = time(0);	// seed to the random number generator
	MTRand rng(rngSeed);					// initialize the random number generator

	const unsigned n = instance.getNumNodes();		// size of chromosomes
	const unsigned p = 100;		// size of population
	const double pe = 0.10;		// fraction of population to be the elite-set
	const double pm = 0.10;		// fraction of population to be replaced by mutants
	const double rhoe = 0.70;	// probability that offspring inherit an allele from elite parent
	const unsigned K = 1;		// number of independent populations
	const unsigned MAXT = 2;	// number of threads for parallel decoding

	// initialize the BRKGA-based heuristic
	BRKGA< PCTSPDecoder, MTRand > algorithm(instance, heuristics, solver, n, p, pe, pm, rhoe, decoder, rng, K, MAXT);

	// BRKGA inner loop (evolution) configuration: Exchange top individuals
	// const unsigned X_INTVL = 100;	// exchange best individuals at every 100 generations
	// const unsigned X_NUMBER = 2;	// exchange top 2 best
	const unsigned MAX_GENS = 500;	// run for 1000 gens

	// BRKGA evolution configuration: restart strategy
	unsigned relevantGeneration = 1;	// last relevant generation: best updated or reset called
	const unsigned RESET_AFTER = 200;
	std::vector< double > bestChromosome;
	double bestFitness = std::numeric_limits< double >::max();

	// Print info about multi-threading:
	#ifdef _OPENMP
		std::cout << "Running for " << MAX_GENS << " generations using " << MAXT
				<< " out of " << omp_get_max_threads()
				<< " available thread units..." << std::endl;
	#endif
	#ifndef _OPENMP
		std::cout << "Running for " << MAX_GENS
				<< " generations without multi-threading..." << std::endl;
	#endif

	// Run the evolution loop:
	unsigned generation = 1;		// current generation
	do {
		algorithm.evolve();	// evolve the population for one generation

		// Bookeeping: has the best solution thus far improved?
		if(algorithm.getBestFitness() < bestFitness) {
			// Save the best solution to be used after the evolution chain:
			relevantGeneration = generation;
			bestFitness = algorithm.getBestFitness();
			bestChromosome = algorithm.getBestChromosome();

			std::cout << "\t" << generation
					<< ") Improved best solution thus far: "
					<< bestFitness << std::endl;
		}

		//  Evolution strategy: restart
		if(generation - relevantGeneration > RESET_AFTER) {
			algorithm.reset();	// restart the algorithm with random keys
			relevantGeneration = generation;

			std::cout << "\t" << generation << ") Reset at generation "
					<< generation << std::endl;
		}

		// Next generation?
		++generation;
	} while (generation < MAX_GENS);

	// print the fitness of the top 10 individuals of each population:
	std::cout << "Fitness of the top 10 individuals of each population:" << std::endl;
	const unsigned bound = std::min(p, unsigned(10));	// makes sure we have 10 individuals
	for(unsigned i = 0; i < K; ++i) {
		std::cout << "Population #" << i << ":" << std::endl;
		for(unsigned j = 0; j < bound; ++j) {
			std::cout << "\t" << j << ") "
					<< algorithm.getPopulation(i).getFitness(j) << std::endl;
		}
	}

	// rebuild the best solution:
	PCTSPSolver bestSolution(instance, bestChromosome);

	// print its distance:
	std::cout << "Best solution found has objective value = "
	 		<< bestSolution.getTourCost() << std::endl;

	// print its best tour:
	std::cout << "Best tour:";
	const std::list< unsigned > bestTour = bestSolution.getTour();
	for(std::list< unsigned >::const_iterator pt = bestTour.begin(); pt != bestTour.end(); ++pt) {
		std::cout << " " << *pt;
	}
	std::cout << std::endl;

	std::cout << "Best tour [random keys]:";
	for(std::vector< double >::const_iterator pt = bestChromosome.begin(); pt != bestChromosome.end(); ++pt) {
		std::cout << " " << *pt;
	}
	std::cout << std::endl;

	const clock_t end = clock();
	std::cout << "BRKGA run finished in " << (end - begin) / double(CLOCKS_PER_SEC) << " s." << std::endl;

    /*
    std::cout << "Press enter to continue ...";
    std::cin.get();
    */
	return 0;
}
